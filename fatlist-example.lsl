//include fatlist-lib.lsl

// --------------------------------------------------------------------------------
// The following functions require per-site editing and are example code of how to
// use a fatlist. N.B. LSL does not have "pass by reference", so you __must__
// use a global variable for your stack or fifo.
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// Fatlist as a queue (FIFO)
// --------------------------------------------------------------------------------

list global_queue = [4];

queue_enqueue(list item)
{
    global_queue = fatlist_insert(global_queue, item, 0);
}

list queue_dequeue()
{
    list item = fatlist_get(global_queue, -1);
    global_queue = fatlist_remove(global_queue, -1);
    return item;
}

// --------------------------------------------------------------------------------
// Fatlist as a stack (LIFO)
// --------------------------------------------------------------------------------

list global_stack = [4];

stack_push(list item)
{
    global_stack = fatlist_append(global_stack, item);
}

list stack_pop()
{
    list item = fatlist_get(global_stack, -1);
    global_stack = fatlist_remove(global_stack, -1);
    return item;
}
