// --------------------------------------------------------------------------------
// Fat List
//
// LSL provides this way to store structured data in a list called "strided list".
// Basically, instead of each list element being a separate item, one treats every
// "n" elements as a separate item. These items are named strides. A strided list
// as LSL has it is a list and an integer. This fat list is different. It is only
// a list with the first element representing the "width" or "stride" of the list.
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// Create a fatlist
// --------------------------------------------------------------------------------

list fatlist_create(integer width)
{
    return [width];
}

list fatlist_from_list(list list_, integer width)
{
    return [width] + list_;
}

// --------------------------------------------------------------------------------
// Get information about a fatlist
// --------------------------------------------------------------------------------

list fatlist_to_list(list fatlist)
{
    return llDeleteSubList(fatlist, 0, 0);
}

integer fatlist_width(list fatlist)
{
    return llList2Integer(fatlist, 0);
}

integer fatlist_count(list fatlist)
{
    integer width = fatlist_width(fatlist);
    list list_ = fatlist_to_list(fatlist);
    return llGetListLength(list_) / width;
}

integer fatlist_validate(list fatlist)
{
    integer width = fatlist_width(fatlist);
    list list_ = fatlist_to_list(fatlist);
    integer length = llGetListLength(list_);
    if (length == -1 || width < 1) {
        return FALSE;
    } else if ((length % width) != 0) {
        return FALSE;
    }
    return TRUE;
}

string fatlist_to_string(list fatlist)
{
    // Build items string
    list items;
    integer count = fatlist_count(fatlist);
    integer current = 0;
    for (; current < count; current++) {
        items += "(" + llDumpList2String(fatlist_get(fatlist, current), ", ") + ")";
    }

    // Build full string representation
    list details = ["<fatlist"];
    details += "width=" + (string)fatlist_width(fatlist);
    details += "count=" + (string)count;
    details += "[" + llDumpList2String(items, "; ") + "]>";
    return llDumpList2String(details, " ");
}

// --------------------------------------------------------------------------------
// Access and modify data in a fatlist
// --------------------------------------------------------------------------------

list fatlist_insert(list fatlist, list item, integer index)
{
    list indexes = fatlist_index_to_list_index(fatlist, index);
    integer start = llList2Integer(indexes, 0);
    return llListInsertList(fatlist, item, start);
}

list fatlist_append(list fatlist, list item)
{
    return fatlist + item;
}

list fatlist_remove(list fatlist, integer index)
{
    list indexes = fatlist_index_to_list_index(fatlist, index);
    integer start = llList2Integer(indexes, 0);
    integer end = llList2Integer(indexes, 1);
    return llDeleteSubList(fatlist, start, end);
}

list fatlist_get(list fatlist, integer index)
{
    list indexes = fatlist_index_to_list_index(fatlist, index);
    integer start = llList2Integer(indexes, 0);
    integer end = llList2Integer(indexes, 1);
    return llList2List(fatlist, start, end);
}

integer fatlist_find(list fatlist, list match)
{
    integer width = fatlist_width(fatlist);
    list list_ = fatlist_to_list(fatlist);
    integer index = llListFindList(list_, match);
    integer offset = index % width;
    if (offset) {
        index -= offset;
    }
    return index;
}

// --------------------------------------------------------------------------------
// Helpers for fatlist
// --------------------------------------------------------------------------------

list fatlist_index_to_list_index(list fatlist, integer index)
{
    integer width = fatlist_width(fatlist);
    if (index < 0) {
        index += fatlist_count(fatlist);
    }
    integer start = 1 + (index * width);
    integer end = start + width - 1;  // Subtract one, otherwise we are off-by-one.
    return [start, end];
}
