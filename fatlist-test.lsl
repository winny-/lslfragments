//include fatlist-lib.lsl

// --------------------------------------------------------------------------------
// Tests
// --------------------------------------------------------------------------------

test_fatlist()
{
    string prefix = "test_fatlist():";

    list l1 = fatlist_create(1);
    list l2 = fatlist_create(2);
    list l3 = fatlist_create(3);

    if (!test_list_equal(l1, [1]) || !test_list_equal(l2, [2]) || !test_list_equal(l3, [3])) {
        lp(prefix, "fatlist_create() failed");
    }

    l1 = fatlist_append(l1, [0]);
    l2 = fatlist_append(l2, [0, 0]);
    l3 = fatlist_append(l3, [0, 0, 0]);

    if (!test_list_equal(l1, [1, 0]) || !test_list_equal(l2, [2, 0, 0]) || !test_list_equal(l3, [3, 0, 0, 0])) {
        lp(prefix, "fatlist_append() failed");
    }

    l1 = fatlist_insert(l1, [1.0], 0);
    l2 = fatlist_insert(l2, [1.0, 1.0], 0);
    l3 = fatlist_insert(l3, [1.0, 1.0, 1.0], 0);

    if (!test_list_equal(l1, [1, 1.0, 0]) || !test_list_equal(l2, [2, 1.0, 1.0, 0, 0]) || !test_list_equal(l3, [3, 1.0, 1.0, 1.0, 0, 0, 0])) {
        lp(prefix, "fatlist_insert() failed");
    }

    if (!test_list_equal(fatlist_get(l1, -1), [0]) || !test_list_equal(fatlist_get(l2, -1), [0, 0]) || !test_list_equal(fatlist_get(l3, -1), [0, 0, 0])) {
        lp(prefix, "fatlist_get() failed");
    }

    lp(prefix, "all tests finished");
}

test_example_queue()
{
    string prefix = "test_example_queue():";
}

test_example_stack()
{
    string prefix = "test_example_stack():";
}


integer test_list_equal(list a, list b)
{
    if (a != b) {
        return FALSE;
    }

    return !llListFindList(a, b);
}

// --------------------------------------------------------------------------------
// Logging
// --------------------------------------------------------------------------------

l(string message)
{
    llOwnerSay(message);
}

lp(string prefix, string message)
{
    l(prefix + " " + message);
}

// --------------------------------------------------------------------------------
// Events
// --------------------------------------------------------------------------------

default
{
    touch_start(integer num_detected)
    {
        test_fatlist();
        test_example_queue();
        test_example_stack();
    }
}
