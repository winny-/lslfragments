// --------------------------------------------------------------------------------
// "Constants"
// --------------------------------------------------------------------------------

string WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
integer CHANNEL = 666666;
integer LOGGING = 0;

// --------------------------------------------------------------------------------
// Globals
// --------------------------------------------------------------------------------

integer listener;
// Each item is two elements wide: (0) avatar UUID (1) HTTPRequest key
list queries;

// --------------------------------------------------------------------------------
// Send and read weather API
// --------------------------------------------------------------------------------

key dispatch_weather_request(string location)
{
    return llHTTPRequest(WEATHER_URL + llEscapeURL(location), [], "");
}

string handle_weather_response(key request_id, integer status, list metadata, string body)
{
    if (status != 200) {
        return "Something went wrong -- HTTP status " + (string)status;
    }
    
    string city_name = llJsonGetValue(body, ["name"]);
    string country = llJsonGetValue(body, ["sys", "country"]);
    country = replace_string(country, "United States of America", "USA");
    string description = llJsonGetValue(body, ["weather", 0, "description"]);
    float kelvins = (float)llJsonGetValue(body, ["main", "temp"]);
    float fahrenheit = kelvins_to_fahrenheit(kelvins);

    return "Weather for " + city_name + ", " + country + ": " + (string)llRound(fahrenheit) + "F " + description;
}

// --------------------------------------------------------------------------------
// Unit conversions
// --------------------------------------------------------------------------------

float kelvins_to_celsius(float kelvin) {
    return kelvin - 273.15;
}

float celsius_to_fahrenheit(float celsius) {
    return (celsius * 2.0) + 30.0;
}

float kelvins_to_fahrenheit(float kelvins) {
    float celsius = kelvins_to_celsius(kelvins);
    return celsius_to_fahrenheit(celsius);
}

// --------------------------------------------------------------------------------
// String manipulation
// --------------------------------------------------------------------------------

string replace_string(string source, string pattern, string replace) {
    while (llSubStringIndex(source, pattern) > -1) {
        integer len = llStringLength(pattern);
        integer pos = llSubStringIndex(source, pattern);

        if (llStringLength(source) == len) {
            source = replace;
        } else if (pos == 0) {
            source = replace+llGetSubString(source, pos+len, -1);
        } else if (pos == llStringLength(source)-len) {
            source = llGetSubString(source, 0, pos-1)+replace;
        } else {
            source = llGetSubString(source, 0, pos-1)+replace+llGetSubString(source, pos+len, -1);
        }
    }
    return source;
}

// ================================================================================
//include fatlist-lib.lsl
// ================================================================================

// --------------------------------------------------------------------------------
// Logging
// --------------------------------------------------------------------------------

log_owner(string message)
{
    if (LOGGING) {
        llOwnerSay(message);
    }
}

// --------------------------------------------------------------------------------
// Events
// --------------------------------------------------------------------------------

default
{
    state_entry()
    {
        log_owner("state_entry() start");
        listener = llListen(CHANNEL, "", "", "");
        queries = fatlist_create(2);
        
        llSetText("Weather Box\nTouch to get weather", <1, 1, 1>, 1);
        llSetTouchText("Weather...");
        log_owner("state_entry() end");
    }
    
    on_rez(integer start_param)
    {
        llResetScript();
    }

    touch_start(integer total_number)
    {
        integer current;
        for (current = 0; current < total_number; current++) {
            llTextBox(llDetectedKey(current), "Enter a city for weather information.", CHANNEL);
        }
        log_owner("touch_start() end");
    }
    
    listen(integer channel, string name, key id, string message)
    {
        string location = llStringTrim(message, STRING_TRIM);
        key http_request_id = dispatch_weather_request(location);

        queries = fatlist_append(queries, [id, http_request_id]);
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        string weather_message = handle_weather_response(request_id, status, metadata, body);

        integer index = fatlist_find(queries, [request_id]);
        if (index == -1) {
            return;
        }
        list item = fatlist_get(queries, index);
        key avatar = llList2Key(item, 0);
        queries = fatlist_remove(queries, index);
        
        llInstantMessage(avatar, weather_message);
    }
}
