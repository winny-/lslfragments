string greeting;
// currentProgress resets to 0 if -1 or if it hits the resetTime
float currentProgress = -1;
// change color ever .05 seconds
float delay = .05;
// How many seconds to wait until resetting the object's floating text
float resetTime = 3;

vector random_color() {
    return  <llFrand(1), llFrand(1), llFrand(1)>;
}

//float in_range(float number, float bottom, float top) {
//    return max(min(number, top), bottom);
//}

blink() {
    if (currentProgress >= resetTime || currentProgress == -1) {
        // currentProgress is either greater or equal to resetTime or hasn't been set yet.
        greeting = llGetObjectName();
        currentProgress = 0;
    } else {
        // increment currentProgress with the delay time.
        currentProgress += delay;
    }

    llSetText(greeting, random_color(), 1);
    integer side;
    for (side = 0; side <= 6; side++) {
        llSetColor(random_color(), side);
    }

    llSetTimerEvent(delay);
}

default
{
    state_entry()
    {
        llSitTarget(<0,0,1>, ZERO_ROTATION);
        blink();
    }

    touch_start(integer total_number)
    {
        greeting = "Hello, "+llDetectedName(0)+"!";
        currentProgress = 0;
    }
    
    timer()
    {
        blink();
    }
}
