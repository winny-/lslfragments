#!/usr/bin/env bash
set -eu

if [[ $# -lt 1 ]]; then
	command=''
else
	command=$1
fi

log() {
	printf "%s\n" "$*"
	"$@"
}


build_lsl() {
	log mkdir -p output
	for f in *.lsl; do
		python lslprocessor.py -o "output/${f%.lsl}.out.lsl" "$f"
	done
}

lint() {
	build_lsl
	for f in output/*.out.lsl; do
		printf -- '--------------------\n'
		log lslint "$f" || true
	done
}

case "$command" in
	clean)
		log rm -rf output
		;;
	lint)
		lint
		;;
	''|build_lsl)
		build_lsl
		;;
	*)
		printf 'Bad command ""\n' "$command"
		exit 1
		;;
esac
