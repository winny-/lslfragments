#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse


def include(parameters):
	if len(parameters) != 1:
		raise ValueError('Bad parameters "{}" for include'.format(parameters))
	with open(parameters[0], 'r') as f:
		return f.read()


prefix = '//'
macros = {
	'include': include,
}


def preprocess_file(input_file, output_file):
	with open(input_file, 'r') as in_f, open(output_file, 'w') as out_f:
		for line in in_f:
			out_f.write(expand_line(line))

				
def expand_line(s):
	scratch = str(s).strip()
	if not s.startswith(prefix):
		return s
	scratch = scratch[len(prefix):]
	try:
		if not scratch[0].isalpha():
			return s
	except IndexError:
		return s
	components = scratch.split(' ')
	macro, parameters = components[0], components[1:]
	valid_macro = set([macro]) & set(macros.keys())
	if not valid_macro:
		print('  Bad macro "{}"'.format(components[0]))
		return s
	return macros[valid_macro.pop()](parameters)


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('file')
	parser.add_argument('-o', '--output', action='store', nargs='?', default=None)

	args = parser.parse_args()
	file_ = args.file
	output = args.output if args.output is not None else file_.replace('.lsl', '.out.lsl')
	print('{} → {}').format(file_, output)
	preprocess_file(file_, output)


if __name__ == '__main__':
	main()
